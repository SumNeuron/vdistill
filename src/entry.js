import Vue from 'vue';

import DAbstract from './components/DAbstract.vue'
import DAppendix from './components/DAppendix.vue'
import DArticle from './components/DArticle.vue'
import DByline from './components/DByline.vue'
import DCitationList from './components/DCitationList.vue'
// import DCite from './components/DCite.vue'
import DContainer from './components/DContainer.vue'
import DFootnoteList from './components/DFootnoteList.vue'
import DHoverBlock from './components/DHoverBlock.vue'
import DistillAcademicCitation from './components/DistillAcademicCitation.vue'
import DistillAppendix from './components/DistillAppendix.vue'
import DistillBibtexCitation from './components/DistillBibtexCitation.vue'
import DistillReuseStatement from './components/DistillReuseStatement.vue'
import DistillUpdatesAndCorrectionsStatement from './components/DistillUpdatesAndCorrectionsStatement.vue'
import DTitle from './components/DTitle.vue'
import DToc from './components/DToc.vue'

const components = {
  DAbstract,
  DAppendix,
  DArticle,
  DByline,
  DCitationList,
  // DCite,
  DContainer,
  DFootnoteList,
  DHoverBlock,
  DistillAcademicCitation,
  DistillAppendix,
  DistillBibtexCitation,
  DistillReuseStatement,
  DistillUpdatesAndCorrectionsStatement,
  DTitle,
  DToc
}



function install(Vue) {
  if (install.installed) return;
  install.installed = true;

  Object.keys(components).forEach(name => {
    Vue.component(name, components[name])
  });

}

const plugin = {
  install,
}

let GlobalVue = null;
if (typeof window !== 'undefined') {
  GlobalVue = window.Vue;
} else if (typeof global !== 'undefined') {
  GlobalVue = global.Vue;
}
if (GlobalVue) {
  GlobalVue.use(plugin);
}


export default components
export const strict = false

export {
  DAbstract,
  DAppendix,
  DArticle,
  DByline,
  DCitationList,
  // DCite,
  DContainer,
  DFootnoteList,
  DHoverBlock,
  DistillAcademicCitation,
  DistillAppendix,
  DistillBibtexCitation,
  DistillReuseStatement,
  DistillUpdatesAndCorrectionsStatement,
  DTitle,
  DToc
}
