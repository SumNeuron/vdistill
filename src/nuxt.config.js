// const repositorySlug = 'vdistill'
// const isGitLabPages  = (process.env.DEPLOY_ENV === 'GL_PAGES')
// const staticLoc = isGitLabPages ? `/${repositorySlug}/` : `/`
// const routerBase = isGitLabPages ? { router: { base: `${staticLoc}` } } : {}
// process.env.DEPLOY_ENV === 'GL_PAGES' || process.env.NODE === 'production'


export default {
  mode: 'spa',
  env: {
    API_VERSION: process.env.API_VERSION || '0',
    PUBLIC_API_URL:process.env.PUBLIC_API_URL || 'http://localhost:6091',
  },
  vue: {
   config: {
     devtools: true
   },
 },
  /*
  ** Headers of the page
  */
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },
  /*
  ** Global CSS
  */
  css: [
  ],
  generate: {
    dir: 'public'
  },
  router: {
    base: process.env.NODE === 'production' ? '/vdistill/' : ''
  },

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
  ],
  /*
  ** Axios module configuration
  ** See https://axios.nuxtjs.org/options
  */
  axios: {
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend (config, ctx) {
    }
  }
}
